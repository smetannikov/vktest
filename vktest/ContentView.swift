//
//  ContentView.swift
//  vktest
//
//  Created by Alexander Smetannikov on 14.07.2022.
//

import SwiftUI
struct ContentView: View {
    @Environment(\.openURL) var openURL
    
    var body: some View {
        
        ZStack {
            Color.black
                .ignoresSafeArea()
            
            VStack{
                Text ("Сервисы VK")
                    .foregroundColor(.white)
                    .font(.title2)
                    .multilineTextAlignment(.center)
                //---------------------
                Button {
                    openURL(URL(string: "https://vk.com/")!)
                } label: {
                    HStack {
                        Image("vk").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("ВКонтакте").foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            
                            Text("Самая популярная соцсеть и первое суперприложение в России")
                                .foregroundColor(.white)
                                .multilineTextAlignment(.leading)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                        
                        
                        
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                
                
                //---------------------
                Button {
                    openURL(URL(string: "https://my.games/")!)
                } label: {
                    HStack {
                        Image("mygames").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("My.Games")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Игры для ПК консолей и смартфонов, в которые играют сотни миллионов геймеров")
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                        
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                    
                }
                
                //---------------------
                Button {
                    openURL(URL(string: "https://sferum.ru/?p=start")!)
                } label: {
                    HStack {
                        Image("sferum").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Сферум")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Онлайн-платформа для обучения и образовательных коммуникаций").multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                            
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://youla.ru/")!)
                } label: {
                    HStack {
                        Image("youla").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Юла")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Сервис объявлений на основе геолокации и интересов")
                                .multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://samokat.ru/")!)
                } label: {
                    HStack {
                        Image("samokat").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Самокат")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Онлайн-ретейлер с доставкой товаров за 15 минут")
                                .foregroundColor(.white)
                                .multilineTextAlignment(.leading)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://citydrive.ru/")!)
                } label: {
                    HStack {
                        Image("citydrive").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Ситидрайв")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Каршеринг-сервис в крупнейших российских городах").multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://cloud.mail.ru/home/")!)
                } label: {
                    HStack {
                        Image("cloud").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Облако")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Сервис для хранения файлов и совместной работы с ними").multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://vseapteki.ru/")!)
                } label: {
                    HStack {
                        Image("apteki").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Все аптеки")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Онлайн-сервис для поиска и щзаказа лекарств по лучшей цене").multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
                //---------------------
                Button {
                    openURL(URL(string: "https://calendar.mail.ru/")!)
                } label: {
                    HStack {
                        Image("calendar").resizable().frame(width: 60, height: 60)
                        VStack(alignment: .leading) {
                            Text("Календарь")
                                .foregroundColor(.white)
                                .font(.title3)
                                .fontWeight(.bold)
                            Text("Планирование дня и эффективное управление временем").multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        Image(systemName: "chevron.right").foregroundColor(.white)
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity,
                        alignment: .topLeading
                    )
                }
            }
        }
        
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
