//
//  vktestApp.swift
//  vktest
//
//  Created by Alexander Smetannikov on 14.07.2022.
//

import SwiftUI

@main
struct vktestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
